package base;

public class Operation1 implements Strategy{
	
	   @Override
	   public void autoPlay() {
		   Main main = new Main(1);
		   main.playerName = "Level 1 Player";
		   main.playGame();
	   }
	   
	}

