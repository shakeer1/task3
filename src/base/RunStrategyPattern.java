package base;


public class RunStrategyPattern {

	public static void main(String[] args) {
	      Context context = new Context(new Operation1());		
	      context.executeStrategy();

	      context = new Context(new Operation2());		
	      context.executeStrategy();

	      context = new Context(new Operation3());		
	      context.executeStrategy();
	   }
	
}

