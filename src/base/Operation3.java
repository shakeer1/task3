package base;

public class Operation3 implements Strategy{
	
	   @Override
	   public void autoPlay() {
		   Main main = new Main(3);
		   main.playerName = "Level 3 Player";
		   main.playGame();
	   }
	   
	}

