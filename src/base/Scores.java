package base;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;

public class Scores {

	void recordTheScore(Main main) {
		try {
			PrintWriter pw = new PrintWriter(new FileWriter("score.txt", true));
			String n = main.playerName.replaceAll(",", "_");
			pw.print(n);
			pw.print(",");
			pw.print(main.score);
			pw.print(",");
			pw.println(System.currentTimeMillis());
			pw.flush();
			pw.close();
		} catch (Exception e) {
			System.out.println("Something went wrong saving scores");
		}
	}

	File readScores() {
		File f = new File("score.txt");
		if (!(f.exists() && f.isFile() && f.canRead())) {
			System.out.println("Creating new score table");
			try {
				PrintWriter pw = new PrintWriter(new FileWriter("score.txt", true));
				pw.print("Hugh Jass");
				pw.print(",");
				pw.print(1500);
				pw.print(",");
				pw.println(1281625395123L);
				pw.print("Ivana Tinkle");
				pw.print(",");
				pw.print(1100);
				pw.print(",");
				pw.println(1281625395123L);
				pw.flush();
				pw.close();
			} catch (Exception e) {
				System.out.println("Something went wrong saving scores");
			}
		}
		return f;
	}

}
