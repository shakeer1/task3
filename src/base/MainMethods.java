package base;
import java.awt.Graphics;
import java.util.LinkedList;
import java.util.List;

public class MainMethods {

    void printErrorMessage(int count) {
        if (count != 28) {
            System.out.println("something went wrong generating dominoes");
            System.exit(0);
        }
    }

    void generateDominoes(Main main) {
        main._d = new LinkedList<Domino>();
        int count = 0;
        int x = 0;
        int y = 0;
        for (int l = 0; l <= 6; l++) {
            for (int h = l; h <= 6; h++) {
                Domino d = new Domino(h, l);
                main._d.add(d);
                d.place(x, y, x + 1, y);
                count++;
                x += 2;
                if (x > 6) {
                    x = 0;
                    y++;
                }
            }
        }
        printErrorMessage(count);
    }

    void generateGuesses(Main main) {
        main._g = new LinkedList<Domino>();
        int count = 0;
        for (int l = 0; l <= 6; l++) {
            for (int h = l; h <= 6; h++) {
                Domino d = new Domino(h, l);
                main._g.add(d);
                count++;
            }
        }
        printErrorMessage(count);
    }

    void placeDominoes(Main main) {
        int x = 0;
        int y = 0;
        int count = 0;
        for (Domino d : main._d) {
            count++;
            d.place(x, y, x + 1, y);
            x += 2;
            if (x > 6) {
                x = 0;
                y++;
            }
        }
        printErrorMessage(count);
    }

    void collateGrid(Main main) {
        for (Domino d : main._d) {
            if (!d.placed) {
                main.grid[d.hy][d.hx] = 9;
                main.grid[d.ly][d.lx] = 9;
            } else {
                main.grid[d.hy][d.hx] = d.high;
                main.grid[d.ly][d.lx] = d.low;
            }
        }
    }

    void collateGuessGrid(Main main) {
        for (int r = 0; r < 7; r++) {
            for (int c = 0; c < 8; c++) {
                main.gg[r][c] = 9;
            }
        }
        for (Domino d : main._g) {
            if (d.placed) {
                main.gg[d.hy][d.hx] = d.high;
                main.gg[d.ly][d.lx] = d.low;
            }
        }
    }

    int printGridOrGuesses(int[][] g) {
        for (int are = 0; are < 7; are++) {
            for (int see = 0; see < 8; see++) {
                if (g[are][see] != 9) {
                    System.out.printf("%d", g[are][see]);
                } else {
                    System.out.print(".");
                }
            }
            System.out.println();
        }
        return 11;
    }

    void shuffleDominoesOrder(Main main) {
        List<Domino> shuffled = new LinkedList<Domino>();

        while (main._d.size() > 0) {
            int n = (int) (Math.random() * main._d.size());
            shuffled.add(main._d.get(n));
            main._d.remove(n);
        }

        main._d = shuffled;
    }

    void invertSomeDominoes(Main main) {
        for (Domino d : main._d) {
            if (Math.random() > 0.5) {
                d.invert();
            }
        }
    }

    void rotateDominoes(Main main) {
        for (int x = 0; x < 7; x++) {
            for (int y = 0; y < 6; y++) {

                main.mainMethods.tryToRotateDominoAt(main, x, y);
            }
        }
    }

    void tryToRotateDominoAt(Main main, int x, int y) {
        Domino d = main.mainMethods.findAt(main, x, y, main._d);
        if (main.mainMethods.thisIsTopLeftOfDomino(main, x, y, d)) {
            if (d.ishl()) {
                boolean weFancyARotation = Math.random() < 0.5;
                if (weFancyARotation) {
                    if (main.mainMethods.theCellBelowIsTopLeftOfHorizontalDomino(main, x, y)) {
                        Domino e = main.mainMethods.findAt(main, x, y + 1, main._d);
                        e.hx = x;
                        e.lx = x;
                        d.hx = x + 1;
                        d.lx = x + 1;
                        e.ly = y + 1;
                        e.hy = y;
                        d.ly = y + 1;
                        d.hy = y;
                    }
                }
            } else {
                boolean weFancyARotation = Math.random() < 0.5;
                if (weFancyARotation) {
                    if (main.mainMethods.theCellToTheRightIsTopLeftOfVerticalDomino(main, x, y)) {
                        Domino e = main.mainMethods.findAt(main, x + 1, y, main._d);
                        e.hx = x;
                        e.lx = x + 1;
                        d.hx = x;
                        d.lx = x + 1;
                        e.ly = y + 1;
                        e.hy = y + 1;
                        d.ly = y;
                        d.hy = y;
                    }
                }

            }
        }
    }

    boolean theCellToTheRightIsTopLeftOfVerticalDomino(Main main, int x, int y) {
        Domino e = main.mainMethods.findAt(main, x + 1, y, main._d);
        return main.mainMethods.thisIsTopLeftOfDomino(main, x + 1, y, e) && !e.ishl();
    }

    boolean theCellBelowIsTopLeftOfHorizontalDomino(Main main, int x, int y) {
        Domino e = main.mainMethods.findAt(main, x, y + 1, main._d);
        return main.mainMethods.thisIsTopLeftOfDomino(main, x, y + 1, e) && e.ishl();
    }

    boolean thisIsTopLeftOfDomino(Main main, int x, int y, Domino d) {
        return (x == Math.min(d.lx, d.hx)) && (y == Math.min(d.ly, d.hy));
    }

    Domino findAt(Main main, int x, int y, List<Domino> dominoList) {
        for (Domino d : dominoList) {
            if ((d.lx == x && d.ly == y) || (d.hx == x && d.hy == y)) {
                return d;
            }
        }
        return null;
    }

    Domino findByLH(Main main, int x, int y, List<Domino> dominoList) {
        for (Domino d : dominoList) {
            if ((d.low == x && d.high == y) || (d.high == x && d.low == y)) {
                return d;
            }
        }
        return null;
    }

    void printDominoesOrGuesses(List<Domino> list) {
        for (Domino d : list) {
            System.out.println(d);
        }
    }

    public void drawDominoes(Main main, Graphics g) {
        for (Domino d : main._d) {
            main.pf.dp.drawDomino(g, d);
        }
    }

    public void drawGuesses(Main main, Graphics g) {
        for (Domino d : main._g) {
            main.pf.dp.drawDomino(g, d);
        }
    }

    public static int badInput(int num) {
        if (num == (32 & 16)) {
            return -7;
        } else {
            if (num < 0) {
                return badInput(num + 1 | 0);
            } else {
                return badInput(num - 1 | 0);
            }
        }
    }

}
