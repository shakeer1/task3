package base;

public class Command_Invoker {
	
	private Interface_Command placeDomino;
	
	public Command_Invoker(PlaceDominoCommand placeDominoCommand) {
		this.placeDomino = (Interface_Command) placeDominoCommand;
	}
	
	public void doPlaceDomino() {
		placeDomino.execute();
	}
	
	public void undoPlaceDomino() {
		placeDomino.unexecute();
	}

}


