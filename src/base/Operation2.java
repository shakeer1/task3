package base;

public class Operation2 implements Strategy{
	
	   @Override
	   public void autoPlay() {
		   Main main = new Main(2);
		   main.playerName = "Level 2 Player";
		   main.playGame();
	   }
	   
	}

