package base;

import java.awt.GridLayout;

import javax.swing.JLabel;

public class Welcome_Menu extends Moniter{

   public Welcome_Menu(Subject subject){
      this.subject = subject;
      this.subject.attach(this);
   }
		
   @Override
   public void displayFrame() {
		welcomeFrame.setLayout(new GridLayout(0, 1));
		label = new JLabel(" Welcome To Abominodo - The Best Dominoes Puzzle Game in the Universe ");
		welcomeFrame.add(label);
		label = new JLabel(" Version 2.0 (c), Kevan Buckley, 2014");
		welcomeFrame.add(label);
		welcomeFrame.pack();
		welcomeFrame.setVisible(true);
   }
   
} 

