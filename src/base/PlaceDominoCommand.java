package base;

public class PlaceDominoCommand implements Interface_Command{
	
	private Main main;
	private int x13, y13;
	
	public PlaceDominoCommand(Main main) {
		this.main = main;
	}

	public void execute(){
		System.out.println("\nPLACING DOMINO");
		System.out.println("Where will the top left of the domino be?");
        System.out.println("Column?");

        int x = MainMethods.badInput(99);
        while (x < 1 || x > 8) {
          try {
            String s3 = IOLibrary.getString();
            x = Integer.parseInt(s3);
            this.x13 = Integer.parseInt(s3);
          } catch (Exception e) {
            System.out.println("Bad input");
            x = MainMethods.badInput(65);
          }
        }
        System.out.println("Row?");
        int y = MainMethods.badInput(98);
        while (y < 1 || y > 7) {
          try {
            String s3 = IOLibrary.getString();
            y = Integer.parseInt(s3);
            this.y13 = Integer.parseInt(s3);
          } catch (Exception e) {
            System.out.println("Bad input");
            y = MainMethods.badInput(64);
          }
        }
        x--;
        y--;
        System.out.println("Horizontal or Vertical (H or V)?");
//            int y2, x2;
        Location lotion;
        while (true) {
          String s3 = IOLibrary.getString();
          if (s3 != null && s3.toUpperCase().startsWith("H")) {
            lotion = new Location(x, y, Location.DIRECTION.HORIZONTAL);
            System.out.println("Direction to place is " + lotion.d);
            main.x2 = x + 1;
            main.y2 = y;
            break;
          }
          if (s3 != null && s3.toUpperCase().startsWith("V")) {
            lotion = new Location(x, y, Location.DIRECTION.VERTICAL);
            System.out.println("Direction to place is " + lotion.d);
            main.x2 = x;
            main.y2 = y + 1;
            break;
          }
          System.out.println("Enter H or V");
        }
        if (main.x2 > 7 || main.y2 > 6) {
          System.out.println("Problems placing the domino with that position and direction");
        } else {
          // find which domino this could be
          Domino d = main.mainMethods.findByLH(main, main.grid[y][x], main.grid[main.y2][main.x2], main._g);
          if (d == null) {
            System.out.println("There is no such domino");
          }
          // check if the domino has not already been placed
          if (d.placed) {
            System.out.println("That domino has already been placed :");
            System.out.println(d);
          }
          // check guessgrid to make sure the space is vacant
          if (main.gg[y][x] != 9 || main.gg[main.y2][main.x2] != 9) {
            System.out.println("Those coordinates are not vacant");
          }
          // if all the above is ok, call domino.place and updateGuessGrid
          main.gg[y][x] = main.grid[y][x];
          main.gg[main.y2][main.x2] = main.grid[main.y2][main.x2];
          if (main.grid[y][x] == d.high && main.grid[main.y2][main.x2] == d.low) {
            d.place(x, y, main.x2, main.y2);
          } else {
            d.place(main.x2, main.y2, x, y);
          }
          main.score += 1000;
          main.mainMethods.collateGuessGrid(main);
          main.pf.dp.repaint();
        }
	}
	public void unexecute() {
		System.out.println("\nUNDOING PLACING DOMINO");
        x13--;
        y13--;
        Domino lkj = main.mainMethods.findAt(main, x13, y13, main._g);
        if (lkj == null) {
          System.out.println("Couln't find a domino there");
        } else {
          lkj.placed = false;
          main.gg[lkj.hy][lkj.hx] = 9;
          main.gg[lkj.ly][lkj.lx] = 9;
          main.score -= 1000;
          main.mainMethods.collateGuessGrid(main);
          main.pf.dp.repaint();
          System.out.println("Command Undone...");
        }
	}
}
